<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Age;

class AgeSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		for($i = 18; $i <= 59; $i++) {
			Age::create(["age" => $i]);
		}
	}
}
