<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\TelegramUser;
use Carbon\Carbon;

class TelegramUserSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$data = [
			[
				"id" => "24537476",
				"first_name" => "Whatyson",
				"last_name" => "Neves",
				"username" => "WhatysonNeves",
				"language_code" => "pt-br",
				"receive" => "1",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now(),
			],
			[
				"id" => "359120480",
				"first_name" => "Eddryni",
				"last_name" => null,
				"username" => "Eddryni",
				"language_code" => "pt-br",
				"receive" => "1",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now(),
			],
			[
				"id" => "907360835",
				"first_name" => "Elaine",
				"last_name" => "Neves",
				"username" => "ArqElaineNeves",
				"language_code" => "pt-br",
				"receive" => "1",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now(),
			],
		];

		TelegramUser::insert($data);
	}
}
