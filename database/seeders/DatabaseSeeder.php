<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Database\Seeders\AgeSeeder;
use Database\Seeders\TelegramUserSeeder;

class DatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		// $this->call('UsersTableSeeder');
		$this->call(AgeSeeder::class);
		$this->call(TelegramUserSeeder::class);
	}
}
