<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTelegramUsersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('telegram_users', function (Blueprint $table) {
			$table->id();
			$table->string('first_name');
			$table->string('last_name')->nullable();
			$table->string('username')->nullable();
			$table->string('language_code')->nullable();
			$table->boolean('receive')->default(true);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('telegram_users');
	}
}
