<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="csrf-token" content="fRIBpmTcPEiemfsPY2GNWeW9e80eYMmWSUclDgf8" />
	<title>Whatyson Neves</title>
	<link rel="stylesheet" type="text/css" href="https://static.whatysonneves.com/assets/css/app.css?id=927dc9f0d1ade3796262" />
	<link rel="stylesheet" type="text/css" href="https://static.whatysonneves.com/assets/css/static.css?id=4798bb289e84e14f2d6d" />
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-117524100-1"></script>
</head>
<body>

	<div class="flex-center position-ref full-height" id="app">
		<div class="content">
			<div class="title m-b-md">
				Vacina DF <small>Bot</small>
			</div>
			<div class="links">
				<a>Saiba primeiro quando chegar a vacinação na sua idade.</a>
				<a href="https://t.me/vacinadfbot">Telegram: @vacinadfbot</a>
				<a href="mailto:contato@whatysonneves.com">contato@whatysonneves.com</a>
			</div>
		</div>
	</div>

</body>
</html>
