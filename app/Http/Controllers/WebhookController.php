<?php

namespace App\Http\Controllers;

use App\Models\TelegramUser;
use App\Models\TelegramWebhookInput;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\TelegramController;

class WebhookController extends Controller
{
	public function error()
	{
		return abort(404);
	}

	public function index(Request $request)
	{
		$json = $this->storeTelegramWebhookInput($request);
		if(!array_key_exists("text", $json["message"])) {
			return response()->json(['status' => true]);
		}
		$create = $this->storeTelegramUser($json["message"]["from"], $json);
		$this->checkReceive($create, $json);
		return response()
			->json(['status' => (bool) $create], ( $create ? 201 : 400 ));
	}

	protected function storeTelegramWebhookInput(Request $request)
	{
		$json = $request->json()->all();
		TelegramWebhookInput::create(["json" => json_encode($json)]);
		return $json;
	}

	protected function storeTelegramUser(array $from, array $json)
	{
		return TelegramUser::updateOrCreate(["id" => $from["id"]], [
			"id" => $from["id"],
			"first_name" => $from["first_name"],
			"last_name" => ( array_key_exists("last_name", $from) ? $from["last_name"] : null ),
			"username" => ( array_key_exists("username", $from) ? $from["username"] : null ),
			"language_code" => ( array_key_exists("language_code", $from) ? $from["language_code"] : null ),
		]);
	}

	protected function checkReceive(TelegramUser $user, array $json)
	{
		$bot = new TelegramController;
		if(preg_match("/^\/start/i", $json["message"]["text"])) {
			$bot->sendMessage($json["message"]["from"]["id"], "Seja bem vindo ao bot.".PHP_EOL."Você irá receber as próximas atualizações sobre as vacinas no DF por aqui.".PHP_EOL.PHP_EOL."Não quer receber mais? Basta enviar /stop.");
			return $user->fill(["receive" => true])->save();
		}
		if(preg_match("/^\/stop/i", $json["message"]["text"])) {
			$bot->sendMessage($json["message"]["from"]["id"], "Obrigado por nos acompanhar até aqui. Se tiver um tempinho, segue o criador do bot lá no Twitter: https://twitter.com/WhatysonNeves.".PHP_EOL.PHP_EOL."Quer voltar a receber? Basta enviar /start.");
			return $user->fill(["receive" => false])->save();
		}
	}
}
