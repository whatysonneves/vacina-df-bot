<?php

namespace App\Http\Controllers;

use TelegramBot\Api\BotApi;

class TelegramController extends Controller
{
	protected $bot = null;
	function __construct()
	{
		$this->bot = new BotApi(env('TELEGRAM_BOT_TOKEN'));
		$this->bot->setCurlOption(CURLOPT_SSL_VERIFYHOST, 0);
		$this->bot->setCurlOption(CURLOPT_SSL_VERIFYPEER, 0);
	}

	public function sendMessage(int $chatId, string $message)
	{
		return $this->bot->sendMessage($chatId, $message);
	}
}
