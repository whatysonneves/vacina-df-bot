<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TelegramWebhookInput extends Model
{
	protected $fillable = [
		"json",
	];
}
